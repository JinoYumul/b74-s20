const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')

const userRoutes = require('./routes/user')

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://jino:jino123@cluster1.7j4kj.mongodb.net/restbooking?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(cors())

app.use('/api/users', userRoutes)

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})