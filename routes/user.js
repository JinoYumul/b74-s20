const express = require('express')
const router = express.Router()
const UserController = require('../controllers/user')

//Check if email exists
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//User Registration
router.post('/', (req, res) => {
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//Login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

module.exports = router