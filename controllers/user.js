const User = require('../models/User')
const bcrypt = require("bcrypt")

//Check if email exists
module.exports.emailExists = (params) => {
	return User.find({email: params.email}).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false
	})
}

//User Registration
module.exports.register = (params) => {
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10) //hashSync() hashes/encrypts and the number is the salt value or how many times the password is hashed
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
		//if there is an error in registration, return false. Otherwise, return true
	})
}

//login
module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(resultFromFindOne => {
		if(resultFromFindOne === null){ //user doesn't exist
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password) //decrypts password

		if(isPasswordMatched){
			//CONTINUE HERE WITH SIR ARVIN
		}
	})
}